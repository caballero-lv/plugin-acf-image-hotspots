<?php
/*
Plugin Name: Advanced Custom Fields: Image Hotspots
Plugin URI: https://www.caballero.lv
Description: Adds Image hotspot field to ACF
Version: 1.0.3
Author: Caballero
Author URI: https://www.caballero.lv
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// check if class already exists.
if ( ! class_exists( 'cab_acf_plugin_image_hotspots' ) ) :

	class cab_acf_plugin_image_hotspots {

		/**
		 * Settings.
		 *
		 * @var array $settings Settings
		 */
		public $settings;


		/**
		 * This function will setup the class functionality
		 *
		 * @type   function
		 * @date   17/02/2016
		 * @since  1.0.0
		 */
		public function __construct() {

			// settings
			// - these will be passed into the field class.
			$this->settings = array(
				'version' => '1.0.3',
				'url'     => plugin_dir_url( __FILE__ ),
				'path'    => plugin_dir_path( __FILE__ ),
			);

			// include field.
			add_action( 'acf/include_field_types', array( $this, 'include_field' ) );
		}


		/**
		 * This function will include the field type class
		 *
		 * @type   function
		 * @date   17/02/2016
		 * @since  1.0.0
		 *
		 * @param int $version major ACF version. Defaults to false.
		 */
		public function include_field( $version = false ) {

			// support empty $version.
			if ( ! $version ) {
				$version = 5;
			}

			// load textdomain.
			load_plugin_textdomain( 'acf-image-hotspots', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' );

			// include.
			include_once 'fields/class-cab-acf-field-image-hotspots.php';
		}

	}


	// initialize.
	new cab_acf_plugin_image_hotspots();


	// class_exists check.
endif;


