(function ($) {
	var ImageHotspots = {
		field: '',

		init: function ($field) {
			ImageHotspots.field = $field;

			ImageHotspots.load();
			ImageHotspots.events();
		},

		load: function () {
			var field = ImageHotspots.field;
			var wrapper = field.find('.image-hotspots-wrap');
			var image = wrapper.find('.image-hotspots__image');

			var pins = wrapper.find('.image-hotspots__pin');
			$.each(pins, function () {
				var pin = $(this);
				ImageHotspots.renderPin(pin, wrapper, image);
			});
		},

		events: function () {
			$('body').on('click', '.image-hotspots-remove-pin', function (e) {
				e.preventDefault();
				var btn = $(this);
				var wrapper = btn.closest('.image-hotspots-wrap');
				ImageHotspots.removePin(wrapper);
				ImageHotspots.saveData(wrapper);
			});

			$('body').on('click', '.image-hotspots-add-pin', function (e) {
				e.preventDefault();
				var btn = $(this);
				var wrapper = btn.closest('.image-hotspots-wrap');
				ImageHotspots.addPin(wrapper);
				ImageHotspots.saveData(wrapper);
			});

			$('body').on('click', '.image-hotspots-select-image', function (e) {
				e.preventDefault();
				var button = $(this);
				ImageHotspots.selectImage(button);
			})
		},

		selectImage: function (button) {
			var _custom_media = true;
			var _orig_send_attachment = wp.media.editor.send.attachment;

			var wrapper = button.closest('.image-hotspots-wrap');

			var send_attachment_bkp = wp.media.editor.send.attachment;
			_custom_media = true;

			wp.media.editor.send.attachment = function (props, attachment) {
				if (_custom_media) {
					wrapper.find('.image-hotspots__image').attr('src', attachment.url);
					wrapper.find('.image-hotspots__image').attr('data-id', attachment.id);
					ImageHotspots.saveData(wrapper);
				} else {
					return _orig_send_attachment.apply(this, [props, attachment]);
				}
			}

			wp.media.editor.open(button);
			return false;
		},

		addPin: function (wrapper) {
			var pins = $(wrapper).find('.image-hotspots__pin');
			var lastNum = Number(pins.last().text());
			var num = lastNum + 1;
			$(wrapper).find('.image-hotspots').append(
				'<div class="image-hotspots__pin" data-top="50" data-left="50" style="top:50%; left:50%">' + num + '</div>'
			);

			var pin = $(wrapper).find('.image-hotspots__pin').last();
			var image = $(wrapper).find('.image-hotspots__image');

			ImageHotspots.renderPin(pin, wrapper, image);
		},

		removePin: function (wrapper) {
			var last = $(wrapper).find('.image-hotspots__pin').last();
			if (last.length) {
				last.remove();
			}
		},

		renderPin: function (pin, wrapper, image) {
			pin.draggable({
				addClasses: false,
				containment: wrapper.children('.image-hotspots'),
				stop: function (event, ui) {
					var top = ui.position.top / image.height() * 100;
					var left = ui.position.left / image.width() * 100;

					pin.attr('data-top', top);
					pin.attr('data-left', left);
					ImageHotspots.saveData(wrapper);
				}
			});
		},

		saveData: function (wrapper) {
			var input = $(wrapper).find('input.image-hotspots-input');
			var value = {
				image_id: wrapper.find('.image-hotspots__image').attr('data-id'),
				pins: []
			};
			$(wrapper).find('.image-hotspots__pin').each(function () {
				var pin = $(this);

				value.pins.push({
					top: pin.attr('data-top'),
					left: pin.attr('data-left'),
				});
			});
			var data = JSON.stringify(value);
			$(input).val(data);
		}
	};


	/**
	 *  ready & append (ACF5)
	 *
	 *  These two events are called when a field element is ready for initizliation.
	 *  - ready: on page load similar to $(document).ready()
	 *  - append: on new DOM elements appended via repeater field or other AJAX calls
	 *
	 *  @param	n/a
	 *  @return	n/a
	 */
	acf.add_action('ready_field/type=image_hotspots', ImageHotspots.init);
	acf.add_action('append_field/type=image_hotspots', ImageHotspots.init);
})(jQuery);