<?php
// exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// check if class already exists.
if ( ! class_exists( 'cab_acf_field_image_hotspots' ) ) :


	class cab_acf_field_image_hotspots extends acf_field {

		/**
		 * This function will setup the field type data
		 *
		 * @type   function
		 * @date   5/03/2014
		 * @since  5.0.0
		 *
		 * @param  n/a
		 * @return n/a
		 */
		function __construct( $settings ) {

			/**
			 * name (string) Single word, no spaces. Underscores allowed
			 */
			$this->name = 'image_hotspots';

			/**
			 * label (string) Multiple words, can include spaces, visible when selecting a field type
			 */
			$this->label = __( 'Image Hotspots', 'acf-image-hotspots' );

			/**
			 * category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
			 */
			$this->category = 'content';

			/**
			 * defaults (array) Array of default settings which are merged into the field object. These are used later in settings
			 */
			$this->defaults = array(
				// 'font_size' => 14,
			);

			/**
			 * l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
			 * var message = acf._e('hotspots', 'error');
			 */
			$this->l10n = array(
				'error' => __( 'Error! Please enter a higher value', 'acf-image-hotspots' ),
			);

			/**
			 * settings (array) Store plugin settings (url, path, version) as a reference for later use with assets
			 */
			$this->settings = $settings;

			// do not delete!
			parent::__construct();
		}


		/**
		 * Create extra settings for your field. These are visible when editing a field
		 *
		 * @type   action
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  $field (array) the $field being edited
		 * @return n/a
		 */
		function render_field_settings( $field ) {

			/**
			 *  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
			 *  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
			 *
			 *  More than one setting can be added by copy/paste the above code.
			 *  Please note that you must also have a matching $defaults value for the field name (font_size)
			 */
			// acf_render_field_setting(
			// $field,
			// array(
			// 'label'        => __( 'Font Size', 'acf-image-hotspots' ),
			// 'instructions' => __( 'Customise the input font size', 'acf-image-hotspots' ),
			// 'type'         => 'number',
			// 'name'         => 'font_size',
			// 'prepend'      => 'px',
			// )
			// );
		}


		/**
		 * Create the HTML interface for your field
		 *
		 * @param  $field (array) the $field being rendered
		 *
		 * @type   action
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  $field (array) the $field being edited
		 * @return n/a
		 */
		public function render_field( $field ) {

			/**
			 *  Review the data of $field.
			 *  This will show what data is available
			 */
			// echo '<pre>';
			// print_r( $field );
			// echo '</pre>';

			$value = json_decode( $field['value'], ARRAY_A );
			if ( empty( $value ) ) :
				$value = array(
					'image_id' => null,
					'pins'     => array(),
				);
			endif;
			?>
			<div class="image-hotspots-wrap">
				<input class="image-hotspots-input hidden"
					type="text"
					name="<?php echo esc_attr( $field['name'] ); ?>"
					value="<?php echo esc_attr( wp_json_encode( $value ) ); ?>"
				>
				<div class="image-hotspots-controls">
					<button class="image-hotspots-select-image button"><?php echo wp_kses_post( __( 'Select image' ) ); ?></button>
				</div>
				<div class="image-hotspots">
					<?php
					if ( $value['image_id'] ) :
						$image = wp_get_attachment_image_src( $value['image_id'], 'large' );
					else :
						$image = null;
					endif;
					?>
					<img class="image-hotspots__image"
						data-id="<?php echo esc_attr( $value['image_id'] ); ?>"
						src="<?php echo esc_url( $image[0] ); ?>"
						alt=""
					>
					<?php
					if ( $value['pins'] ) :
						$pins = $value['pins'];
						foreach ( $pins as $index => $pin ) :
							if ( $pin ) :
								$num   = $index + 1;
								$style = 'top:' . $pin['top'] . '%;left:' . $pin['left'] . '%;'
								?>
								<div class="image-hotspots__pin"
									data-top="<?php echo esc_attr( $pin['top'] ); ?>"
									data-left="<?php echo esc_attr( $pin['left'] ); ?>"
									style="<?php echo esc_attr( $style ); ?>"
								><?php echo esc_textarea( $num ); ?></div>
								<?php
							endif;
						endforeach;
					endif;
					?>
				</div>
				<div class="image-hotspots-pin-controls">
					<button class="button image-hotspots-add-pin"><?php echo wp_kses_post( __( 'Add pin' ) ); ?></button>
					<button class="button image-hotspots-remove-pin"><?php echo wp_kses_post( __( 'Remove pin' ) ); ?></button>
				</div>
			</div>
			<?php
		}


		/**
		 * This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
		 * Use this action to add CSS + JavaScript to assist your render_field() action.
		 *
		 * @type   action (admin_enqueue_scripts)
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  n/a
		 * @return n/a
		 */
		function input_admin_enqueue_scripts() {
			// vars.
			$url     = $this->settings['url'];
			$version = $this->settings['version'];

			// register & include JS.
			wp_register_script( 'acf-image-hotspots', "{$url}assets/js/input.js", array( 'acf-input' ), $version );
			wp_enqueue_script( 'acf-image-hotspots' );

			// register & include CSS.
			wp_register_style( 'acf-image-hotspots', "{$url}assets/css/input.css", array( 'acf-input' ), $version );
			wp_enqueue_style( 'acf-image-hotspots' );
		}


		/**
		 * This action is called in the admin_head action on the edit screen where your field is created.
		 * Use this action to add CSS and JavaScript to assist your render_field() action.
		 *
		 * @type   action (admin_head)
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  n/a
		 * @return n/a
		 */
		// function input_admin_head() {}


		/**
		 * This function is called once on the 'input' page between the head and footer
		 * There are 2 situations where ACF did not load during the 'acf/input_admin_enqueue_scripts' and
		 * 'acf/input_admin_head' actions because ACF did not know it was going to be used. These situations are
		 * seen on comments / user edit forms on the front end. This function will always be called, and includes
		 * $args that related to the current screen such as $args['post_id']
		 *
		 * @type   function
		 * @date   6/03/2014
		 * @since  5.0.0
		 *
		 * @param  $args (array)
		 * @return n/a
		 */
		// function input_form_data( $args ) {}


		/**
		 * This action is called in the admin_footer action on the edit screen where your field is created.
		 * Use this action to add CSS and JavaScript to assist your render_field() action.
		 *
		 * @type   action (admin_footer)
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  n/a
		 * @return n/a
		 */
		// function input_admin_footer() {}


		/**
		* This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
		* Use this action to add CSS + JavaScript to assist your render_field_options() action.
		*
		* @type   action (admin_enqueue_scripts)
		* @since  3.6
		* @date   23/01/13
		*
		* @param  n/a
		* @return n/a
		*/
		// function field_group_admin_enqueue_scripts() {}


		/**
		 * This action is called in the admin_head action on the edit screen where your field is edited.
		 * Use this action to add CSS and JavaScript to assist your render_field_options() action.
		 *
		 * @type   action (admin_head)
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  n/a
		 * @return n/a
		 */
		// function field_group_admin_head() {}


		/**
		 * This filter is applied to the $value after it is loaded from the db
		 *
		 * @type   filter
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  $value (mixed) the value found in the database
		 * @param  $post_id (mixed) the $post_id from which the value was loaded
		 * @param  $field (array) the field array holding all the field options
		 * @return $value
		 */
		/*
		function load_value( $value, $post_id, $field ) {
			return $value;
		}
		*/


		/**
		 * This filter is applied to the $value before it is saved in the db
		 *
		 * @type    filter
		 * @since   3.6
		 * @date    23/01/13
		 *
		 * @param  $value (mixed) the value found in the database
		 * @param  $post_id (mixed) the $post_id from which the value was loaded
		 * @param  $field (array) the field array holding all the field options
		 * @return $value
		 */
		/*
		function update_value( $value, $post_id, $field ) {
			return $value;
		}
		*/

		/**
		 * This filter is appied to the $value after it is loaded from the db and before it is returned to the template
		 *
		 * @type   filter
		 * @since  3.6
		 * @date   23/01/13
		 *
		 * @param  $value (mixed) the value which was loaded from the database
		 * @param  $post_id (mixed) the $post_id from which the value was loaded
		 * @param  $field (array) the field array holding all the field options
		 *
		 * @return $value (mixed) the modified value
		 */
		/*
		function format_value( $value, $post_id, $field ) {
			// bail early if no value
			if( empty($value) ) {
				return $value;
			}
			// apply setting
			if( $field['font_size'] > 12 ) {
				// format the value
				// $value = 'something';
			}
			// return
			return $value;
		}
		*/


		/**
		 * This filter is used to perform validation on the value prior to saving.
		 * All values are validated regardless of the field's required setting. This allows you to validate and return
		 * messages to the user if the value is not correct
		 *
		 * @type   filter
		 * @date   11/02/2014
		 * @since  5.0.0
		 *
		 * @param  $valid (boolean) validation status based on the value and the field's required setting
		 * @param  $value (mixed) the $_POST value
		 * @param  $field (array) the field array holding all the field options
		 * @param  $input (string) the corresponding input name for $_POST value
		 * @return $valid
		 */
		/*
		function validate_value( $valid, $value, $field, $input ) {

			// Basic usage
			if( $value < $field['custom_minimum_setting'] )
			{
				$valid = false;
			}

			// Advanced usage
			if( $value < $field['custom_minimum_setting'] )
			{
				$valid = __('The value is too little!','acf-image-hotspots'),
			}

			// return
			return $valid;
		}
		*/


		/**
		 * This action is fired after a value has been deleted from the db.
		 * Please note that saving a blank value is treated as an update, not a delete
		 *
		 * @type   action
		 * @date   6/03/2014
		 * @since  5.0.0
		 *
		 * @param  $post_id (mixed) the $post_id from which the value was deleted
		 * @param  $key (string) the $meta_key which the value was deleted
		 * @return n/a
		 */
		// function delete_value( $post_id, $key ) {}


		/**
		 * This filter is applied to the $field after it is loaded from the database
		 *
		 * @type   filter
		 * @date   23/01/2013
		 * @since  3.6.0
		 *
		 * @param  $field (array) the field array holding all the field options
		 * @return $field
		 */
		/*
		function load_field( $field ) {
			return $field;
		}
		*/


		/**
		 * This filter is applied to the $field before it is saved to the database
		 *
		 * @type   filter
		 * @date   23/01/2013
		 * @since  3.6.0
		 *
		 * @param  $field (array) the field array holding all the field options
		 * @return $field
		 */
		/*
		function update_field( $field ) {
			return $field;
		}
		*/

		/**
		 *  This action is fired after a field is deleted from the database
		 *
		 * @type   action
		 * @date   11/02/2014
		 * @since  5.0.0
		 *
		 * @param  $field (array) the field array holding all the field options
		 * @return n/a
		 */
		// function delete_field( $field ) {}
	}

	// initialize.
	new cab_acf_field_image_hotspots( $this->settings );

	// class_exists check.
endif;
