=== Advanced Custom Fields: Image Hotspots Field ===
Contributors: Caballero
Tags: acf, custom fields, fields, meta
Requires at least: 5.0
Tested up to: 5.5.1
Stable tag: master
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds Image hotspot field to ACF

== Description ==

EXTENDED_DESCRIPTION

= Compatibility =

This ACF field type is compatible with:
* ACF 5

== Installation ==

1. Copy the `acf-image-hotspots` folder into your `wp-content/plugins` folder
2. Activate the Image Hotspots plugin via the plugins admin page
3. Create a new field via ACF and select the Image Hotspots type
4. Read the description above for usage instructions

== Changelog ==

= 1.0.3 =
* Fixed pin appearance in admin

= 1.0.2 =
* Version fix

= 1.0.1 =
* Fixed bug where pins cant be moved after save

= 1.0.0 =
* Initial Release.